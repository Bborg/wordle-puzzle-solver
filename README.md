# Wordle Puzzle Solver

Solution for wordle, a game similar to mastermind where you get 6 guesses to find the correct 5-letter word.

Upon each guess you are told whether any letters in your guess are in the answers (but not in the right place)
and whether any letters are correct and in the right place.

The above python script finds out how often each character (letter) occurs in the set of possible solutions,
the first guess is static 'orate' the only word within the solution set containing the 5 most commonly occuring
letters. After the first guess we filter out any words containing letters which we know aren't in the answer and
all words which although contain correct letters they are however in positions we already tried and hence can be
excluded from the pool of possible solutions. From the new filtered list of possible solutions we select the one
with the highest score (most common letters) as our next guess. From here the procedure repeats till a correct
answer is found or it runs out of tries and hence fails.

The average success rate is about 93% the average number of steps taken to get a correct answer is about 3.



