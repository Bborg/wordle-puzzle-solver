from operator import truediv
import random
from collections import Counter

def filterSolutions(exist,discard,solutions):
    filteredSolutions=[]

    for i in solutions:
        x = True
        for j in discard:
            if(j in i ):
                x = False
        for j in exist:
            temp =exist[j]
            for k in temp:
                if i[k] == j:
                    x=False


        if(x == True):
            filteredSolutions.append(i)
    i
    return filteredSolutions

def filterWords(exist, correctPos,solutions):
    filteredDict={}
    existDict={}
    correctPosDict={}

    for i in solutions:
        
        for j,x in enumerate(exist):
            existDict[exist[j]]=False

        for j,x in enumerate(correctPos):
            if correctPos[j] != '':
                correctPosDict[correctPos[j]] = False

        for j, x in enumerate(i):
            if (correctPos[j] != '') and (i[j]==correctPos[j]):
                    correctPosDict[x]=True
            if x in existDict.keys():
                existDict[x]=True
        
        checkValid = True
        for j, x in enumerate(correctPosDict):
            checkValid = checkValid and correctPosDict[x]
        for j,x in enumerate(existDict):
            checkValid = checkValid and existDict[x]
        
        if(checkValid == True):
            temp =0
            for j, x in enumerate(i):
                temp += occuranceDict[x]
            filteredDict[i]=temp

    c=Counter(filteredDict)
    filteredDict=c

    filteredList = [(k, v) for k, v in filteredDict.items()]
    filteredList.sort(key=lambda x:x[1],reverse=True)
    print(filteredList)
    return filteredList[0][0]

def SolveWordle(solutions):
    #Set answers and first guess along with arrays to take future guesses
    ans = random.choice(solutions)
    #ans = 'melee'
    firstGuess = 'orate'
    print(ans)
    #Takes first guess and populates exist and correctPos accordingly
    for k in range(6):
        exist={}
        correctPos = ['']*5 
        discard = []
        print(k)
        if firstGuess == ans:
            return (True, k)
        for i, x in enumerate(firstGuess):
            for j, y in enumerate(ans):
                if(x==y and i==j):
                    correctPos[i]=ans[j]
                    if(x in exist.keys()):
                        if(i in exist[x]):
                            temp = exist[x]
                            temp.remove(i)
                            exist[x] =temp

                elif(x==y and(x not in correctPos)):
                    if(x in exist.keys()):
                        temp = exist[x]
                        if(i not in exist[x]):
                            temp.append(i)
                        exist[x] =temp
                        print(exist[x])
                    else:
                        exist[x]=[i]
        
        existList = list(exist.keys())
        print(exist)
        print(correctPos)
        for i in firstGuess:                                                                   

            if (i in exist) or (i in correctPos):
                continue
            else:
                discard.append(i)
        
        print(discard)

        solutions = filterSolutions(exist,discard,solutions)   
        firstGuess = filterWords(existList,correctPos,solutions)
    
    return (False,k)

#Importing data from files and populating the respective arrays
with open('wordle-allowed-guesses.txt') as guesses:
    guessTemp = guesses.readlines()

with open('wordle-answers-alphabetical.txt') as answers:
    solutionsTemp = answers.readlines()

solutions = []
superSet = []

for i in guessTemp:
    superSet.append(i.strip())

for i in solutionsTemp:
    solutions.append(i.strip())

for i in solutions:
    superSet.append(i)



#Dictionary of letters sorted by how often they occur.
occuranceDict= {}
for i in solutions:
    for j, x in enumerate(i):
        if x in occuranceDict.keys():
            occuranceDict[x] +=  1
        else:
            occuranceDict[x] = 0
c=Counter(occuranceDict)
occuranceDict=c

x=[]

for i in range(1000):
    temp = SolveWordle(solutions)
    x.append(temp)


avgSteps = 0
successRate = 0
for i in x:
    avgSteps += i[1]
    if (i[0]==True):
        successRate += 1

avgSteps /= 1000
successRate/=1000

print("Average number of steps: " + str(avgSteps))
print("Success rate: " + str(successRate))



#TODO: Fix empty list bug 